odoo.define("mocphat_qr_code.qr_code", function (require) {
    "use strict";

    var core = require('web.core');
    var session = require('web.session');
    var ajax = require('web.ajax');
    var Widget = require('web.Widget');
    var ControlPanelMixin = require('web.ControlPanelMixin');
    var QWeb = core.qweb;
    var _t = core._t;
    var _lt = core._lt;
    var rpc = require('web.rpc');
    let scanning = false;
    qrcode = window.qrcode;
    const video = document.createElement("video");

    var MpQrCode = Widget.extend(ControlPanelMixin, {
      template: "action_qr_code_header_v1",
      events: {
        'click #btn-scan-qr': '_click_barcode',
        'click .mo_modal_done': '_get_id_mo_modal_done',
        'click .confirm_done': '_confirm_done',
      },

      init: function(parent, context) {
        this._super(parent, context);
        var self = this;
        self.action_id = context.id;
        self.tag_name = context.tag;
        this._super(parent, context);
        this.tick = this.tick.bind(this);
        this.scan = this.scan.bind(this);
        self.test = 'áhdashdsahd';
      },
      willStart: function() {
        return $.when(ajax.loadLibs(this), this._super());
      },

      _click_barcode: function(){
        var self = this;
        const canvasElement = document.getElementById("qr-canvas");
        const qrResult = document.getElementById("qr-result");
        const btnScanQR = document.getElementById("btn-scan-qr");
        navigator.mediaDevices
        .getUserMedia({ video: { facingMode: "environment" } })
        .then(function(stream) {
          scanning = true;
          qrResult.hidden = true;
          btnScanQR.hidden = true;
          canvasElement.hidden = false;
          video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
          video.srcObject = stream;
          video.play();
          self.tick();
          self.scan();
        });
      },

      tick: function(){
        var self = this;
          const canvasElement = document.getElementById("qr-canvas");
          canvasElement.height = video.videoHeight;
          canvasElement.width = video.videoWidth;
          const canvas = canvasElement.getContext("2d");
          canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);

          scanning && requestAnimationFrame(this.tick);
      },
      scan: function(){
        var self = this;
        const outputData = document.getElementById("outputData");
        const qrResult = document.getElementById("qr-result");
        const canvasElement = document.getElementById("qr-canvas");
        const btnScanQR = document.getElementById("btn-scan-qr");
        const xuatKho = document.getElementById("xuat_kho");
        const sanPham = document.getElementById("san_pham");
        const keSanPham = document.getElementById("ke_san_pham");
        try {
            qrcode.decode();
            outputData.innerText = qrcode.result;
            qrResult.hidden = false;
            canvasElement.hidden = true;
            btnScanQR.hidden = false;
            const textData = qrcode.result.split('-');

            if (qrcode.result.split('-').length > 1){
                if (textData[0] == 'stock_picking'){
                    xuat_kho.innerText = qrcode.result;
                }
                if (textData[0] == 'stock_production_lot'){
                    sanPham.innerText = qrcode.result;
                }
                if (textData[0] == 'stock_location'){
                    keSanPham.innerText = qrcode.result;
                }
            }
            const res = $('#xuat_kho').html().split('-')[1];
            const res1 = $('#san_pham').html().split('-')[1];
            const res2 = $('#ke_san_pham').html().split('-')[1];
            let nameStock = [res, res1, res2];
            self.get_data_stock_move_line(nameStock);
          } catch (e) {
            setTimeout(this.scan, 300);
          }
      },

      get_data_stock_move_line: function(stock_name){
            var self = this;
            rpc.query({
              model: 'stock.picking',
              method: 'generate_data_inventory',
              args: [stock_name]
            }).then(function(result) {
                self.get_all_data = result[0];
                self.name_inventory = result[1];
                self.partner = result[2];
                var data_move_line = document.getElementById('action_data_move_line_ids')
                var not_data = document.getElementById('action_not_data')
                if (data_move_line){
                    data_move_line.remove()
                }
                if (not_data){
                    not_data.remove()
                }
                if(result.length >= 1){
                    $('#action_qr_code_header_v1').append(QWeb.render('action_data_move_line_ids', { widget: self }));
                }
                else{
                    $('#action_qr_code_header_v1').append(QWeb.render('action_not_data', { widget: self }));
                }
            })
      },

      _get_id_mo_modal_done: function(events ){
        var self = this;
        const widgetForm = events.target.closest(".widget_form");
        const getIdMoveLineElement = widgetForm.querySelector(".hide_quantity");
        const so_luong_xi = widgetForm.querySelector(".so_luong_xi");
        const quantity = widgetForm.querySelector(".quantity");
        const moveLine = parseInt(getIdMoveLineElement.value);
        localStorage.setItem('modal_move_line_done_id', moveLine);
        localStorage.setItem('so_luong_xi', parseInt(so_luong_xi.value));
        localStorage.setItem('so_luong_quet', parseInt(quantity.value));
      },

      _confirm_done: function(events){
        var self = this;
        const moveLine = localStorage.getItem('modal_move_line_done_id');
        const so_luong_xi = localStorage.getItem('so_luong_xi');
        const so_luong_quet = localStorage.getItem('so_luong_quet');
        if (parseInt(so_luong_xi) != parseInt(so_luong_quet)){
            alert('Số lượng quét không bằng lượng xí');
        }
        else{
            rpc.query({
              model: 'stock.picking',
              method: 'push_number_of_scans',
              args: [moveLine, so_luong_quet]
            }).then(function(result) {
                $('#exampleModal').modal('hide');
            })
        }
      },

      start: function() {
          this._super()
          var self = this;


      },
      reload: function () {
          self.render();
      },
    });

    core.action_registry.add('mocphat_qr_code.qr_code', MpQrCode);

    return MpQrCode;

    });





{
    'name': 'MocPhat QR CODE',
    'summary': 'GENERATE QR CODE',
    'category': 'Extra Tools',
    'description': """
MocPhat Reports
==================
    """,
    'author': 'MOC PHAT CO.,LTD',
    'website': 'https://www.mocphat.com',
    'depends': [
        'stock',
        'mocphat'
    ],
    'data': [
        # ========================== SECURITY ==============================
        # ========================== MENU ==============================
        'menu/qr_code_menu.xml',
        # 'menu/employee_machine/employee_machine.xml',
        # ========================== VIEW ==============================
        'views/template.xml',
        'views/stock_picking.xml',
        # ========================== Wizard ==============================
        # ========================== Reports ==============================
    ],
    'qweb': [
        'static/src/xml/qr_code.xml',
    ],
    # 'auto_install': True,
    'installable': True,
    'application': True,
    # 'license': 'OEEL-1',
}

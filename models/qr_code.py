from odoo import models, fields, api, tools, _
from odoo.tools.misc import format_date
from collections import Counter
from babel.numbers import format_currency
from babel.numbers import format_decimal
from datetime import date, timedelta, datetime
from odoo.exceptions import UserError
import json

class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    number_of_scans = fields.Integer('scans')


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.model
    def generate_data_inventory(self, stock_name):
        arr = []
        if not stock_name[0]:
            return []
        check_stock = self.env['stock.picking'].browse(int(stock_name[0]))
        if not check_stock:
            return []
        # name: [stock, lot, location]
        where_clause = ''
        if not stock_name[1] and not stock_name[2]:
            where_clause = 'where sp.id = %s' % (stock_name[0])
        if stock_name[1] and not stock_name[2]:
            where_clause = 'where sp.id = %s and spl.id = %s' % (stock_name[0], stock_name[1])
        if stock_name[2] and not stock_name[1]:
            where_clause = 'where sp.id = %s and sl.id = %s' % (stock_name[0], stock_name[2])
        if stock_name[1] and stock_name[2]:
            where_clause = 'where sp.id = %s and spl.id = %s and sl.id = %s' % (stock_name[0], stock_name[1], stock_name[2])
        sql = """
            select pr.id, sl.name, spl.name, sml.product_uom_qty, sml.id from stock_picking sp
                join stock_move_line sml on sml.picking_id = sp.id
                join product_product pr on sml.product_id = pr.id
                join stock_production_lot spl on sml.lot_id = spl.id
                join stock_location sl on sml.location_id = sl.id
            %s
            group by pr.id, sl.name, spl.name, sml.product_uom_qty, sml.id
        """ % where_clause
        self.env.cr.execute(sql)
        res_all = self.env.cr.fetchall()
        for index, item in enumerate(res_all):
            product = self.env['product.product'].browse(int(item[0]))
            val = {
                'stt': index+1,
                'product_id': '[%s] - %s' % (product.default_code, product.name),
                'location_id': item[1],
                'lot_id': item[2],
                'product_uom_qty': item[3],
                'id': item[4],
            }
            arr.append(val)
        return arr, check_stock.name, check_stock.partner_id.name

    @api.model
    def push_number_of_scans(self, move_line_id, quanity):
        move_line = self.env['stock.move.line'].browse(int(move_line_id))
        if move_line:
            move_line.number_of_scans = quanity

